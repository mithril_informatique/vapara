class AddRefToProduct < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :ref, :string
  end
end
