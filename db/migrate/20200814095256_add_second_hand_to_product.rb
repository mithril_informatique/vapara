class AddSecondHandToProduct < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :second_hand, :boolean, default: false
  end
end
