class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :title
      t.text :short_description
      t.text :long_description
      t.references :category, null: false, foreign_key: true
      t.integer :stock
      t.float :weight
      t.string :dimensions
      t.integer :dolibarr_id

      t.timestamps
    end
  end
end
