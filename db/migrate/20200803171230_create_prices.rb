class CreatePrices < ActiveRecord::Migration[6.0]
  def change
    create_table :prices do |t|
      t.references :product, null: false, foreign_key: true
      t.references :currency, null: false, foreign_key: true
      t.float :ht
      t.float :ttc

      t.timestamps
    end
  end
end
