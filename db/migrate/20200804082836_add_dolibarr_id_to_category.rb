class AddDolibarrIdToCategory < ActiveRecord::Migration[6.0]
  def change
    add_column :categories, :dolibarr_id, :integer
  end
end
