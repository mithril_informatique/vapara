# frozen_string_literal: true

# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

CURRENCY_ML = Currency.where(name: 'June').first
CURRENCY_UNL = Currency.where.not(name: 'June').first
