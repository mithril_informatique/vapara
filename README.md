# Vapāra

## Database

### Product

* id => integer
* title => string
* short_description => text
* long_description => text
* category_id => references
* stock => integer
* weight => integer
* dimensions => string
* dolibarr_id => integer

### Category

* id => integer
* title => string
* parent => integer

### Currency

* id => integer
* symbol => string
* name => string

### Price

* id => integer
* product_id => references
* currency_id => references
* ht => float
* ttc => float