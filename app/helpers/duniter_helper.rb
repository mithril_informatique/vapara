module DuniterHelper
  def search_tx(amount, invoice_number)
    duniter_key = 'Bf9PttKSME9PEDvLdVVnXRHLEDS1vosVufWPDbmgtJJu'
    receive(duniter_key).each do |rec|
      next unless rec[:comment].downcase.include?(invoice_number.downcase)

      rec[:outputs].each do |tx|
        transaction = tx.split(':')
        tx_amount = transaction[0].to_f / 100
        from_key = rec[:issuers].first
        if tx.include?(duniter_key) && tx_amount == amount
          return { blockstamp: rec[:blockstamp],
                   blockstamp_time: rec[:blockstampTime],
                   comment: rec[:comment],
                   amount: tx_amount,
                   from_key: from_key }
        end
      end
    end
    false
  end

  def receive(duniter_key)
    response = RestClient.get("https://g1.mithril.re/tx/history/#{duniter_key}")
    history = eval(response.gsub('null', 'nil'))
    history[:history][:received]
  end

  def uid(duniter_key)
    response = RestClient.get("https://g1.mithril.re/blockchain/memberships/#{duniter_key}")
    membership = eval(response.gsub('null', 'nil'))
    membership[:uid]
  end
end
