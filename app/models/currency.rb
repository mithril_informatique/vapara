class Currency < ApplicationRecord
  validates :name, :symbol, presence: true
  has_many :prices, dependent: :delete_all
end
