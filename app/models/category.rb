class Category < ApplicationRecord
  validates :title, presence: true
  validates :title, uniqueness: true
  has_many :products, dependent: :delete_all
end
