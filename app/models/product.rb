class Product < ApplicationRecord
  belongs_to :category
  validates :title, presence: true
  validates :title, uniqueness: true
  has_many :prices, dependent: :delete_all
  has_many_attached :images, dependent: :delete_all

  before_destroy do
    images.purge
    prices.delete_all
  end

  def price_ml
    prices.where(currency_id: CURRENCY_ML.id).first
  end
end
