class Price < ApplicationRecord
  belongs_to :product
  belongs_to :currency
  validates :ht, :ttc, presence: true
end
