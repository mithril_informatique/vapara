json.extract! product, :id, :title, :long_description, :category_id, :stock, :weight, :dimensions, :dolibarr_id, :url, :ref, :second_hand, :created_at, :updated_at
