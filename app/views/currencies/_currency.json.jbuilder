json.extract! currency, :id, :symbol, :name, :created_at, :updated_at
json.url currency_url(currency, format: :json)
