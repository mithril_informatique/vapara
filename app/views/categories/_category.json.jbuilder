json.extract! category, :id, :title, :parent, :created_at, :updated_at
json.url category_url(category, format: :json)
