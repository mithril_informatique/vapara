json.extract! price, :id, :product_id, :currency_id, :ht, :ttc, :created_at, :updated_at
json.url price_url(price, format: :json)
