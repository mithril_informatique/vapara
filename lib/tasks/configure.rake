# frozen_string_literal: true

namespace :configure do
  DOLIBARR_CONF_FILE = Rails.root.join('config', 'dolibarr.yml')
  desc 'Create dolibarr.yml config file'
  task dolibarr: :environment do
    puts 'Préparation de la liaison avec Dolibarr'
    conf = File.exist?(DOLIBARR_CONF_FILE) ? YAML.load_file(DOLIBARR_CONF_FILE) : {}
    puts "Url de Dolibarr [#{conf['url']}] : "
    url = STDIN.gets.chomp
    conf['url'] = url unless url.empty?
    puts "Dolapiley [#{conf['dolapikey']}] : "
    dolapikey = STDIN.gets.chomp
    conf['dolapikey'] = dolapikey unless dolapikey.empty?
    File.write(DOLIBARR_CONF_FILE, conf.to_yaml)
    puts 'Configuration terminée'
  end

  desc 'Create currencies'
  task currencies: :environment do
    puts 'No yet implemented'
  end
end
